package com.azinova.network

import com.azinova.model.input.dashboard.InputDashboard
import com.azinova.model.input.expense_add.InputExpenseAdd
import com.azinova.model.input.history.InputHistory
import com.azinova.model.input.last_voucher.InputLastVoucher
import com.azinova.model.input.location_update.InputLocationUpdate
import com.azinova.model.input.login.InputLoginApi
import com.azinova.model.input.payment.InputPay
import com.azinova.model.input.report.InputReport
import com.azinova.model.input.resend_sms.InputResendSms
import com.azinova.model.input.saleVoucher.InputSaleVoucher
import com.azinova.model.input.users.InputGetUser
import com.azinova.model.input.validity.InputValidity
import com.azinova.model.response.dashboard.ResponseDashboard
import com.azinova.model.response.expense_add.ResponseExpenseAdd
import com.azinova.model.response.expense_category.ResponseExpenseCategory
import com.azinova.model.response.expense_list.ResponseExpenseList
import com.azinova.model.response.history.ResponseHistory
import com.azinova.model.response.last_voucher.ResponseLastVoucher
import com.azinova.model.response.location_update.ResponseLocationUpdate
import com.azinova.model.response.login.ResponseLogin
import com.azinova.model.response.payment.ResponsePayment
import com.azinova.model.response.payment_list.ResponsePaymentList
import com.azinova.model.response.report.ResponseReport
import com.azinova.model.response.resend_sms.ResponseResendSms
import com.azinova.model.response.saleVoucher.ResponseSaleVoucher
import com.azinova.model.response.users.ResponseGetUser
import com.azinova.model.response.validity.ResponseValidity
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {

    @POST("login?ws=smartwifi985698256ad256")
    suspend fun login(@Body inputLoginApi: InputLoginApi):ResponseLogin

    @POST("voucher_sale?ws=smartwifi985698256ad256")
    suspend fun createSale(@Body inputsaleVoucher: InputSaleVoucher):ResponseSaleVoucher

    @POST("dashboard?ws=smartwifi985698256ad256")
    suspend fun dashBoard(@Body inputDashboard: InputDashboard):ResponseDashboard

    @POST("last_voucher_detail?ws=smartwifi985698256ad256")
    suspend fun lastVoucher(@Body inputLastVoucher: InputLastVoucher):ResponseLastVoucher

    @POST("voucher_history?ws=smartwifi985698256ad256")
    suspend fun history(@Body inputHistory: InputHistory):ResponseHistory

    @POST("resend_voucher_sms?ws=smartwifi985698256ad256")
    suspend fun resendSms(@Body inputResendSms: InputResendSms):ResponseResendSms

    @POST("voucher_sales_report?ws=smartwifi985698256ad256")
    suspend fun salesReport(@Body inputReport: InputReport): ResponseReport

    @POST("list_hotspot_users?ws=smartwifi985698256ad256")
    suspend fun getUser(@Body inputGetUser: InputGetUser): ResponseGetUser

    @POST("add_payment?ws=smartwifi985698256ad256")
    suspend fun pay(@Body inputPay: InputPay): ResponsePayment

    @POST("list_validity_profiles?ws=smartwifi985698256ad256")
    suspend fun getValidity(@Body inputValidity: InputValidity): ResponseValidity

    @POST("list_payments?ws=smartwifi985698256ad256")
    suspend fun getListPayments(@Body inputValidity: InputReport): ResponsePaymentList

    @POST("expense_category?ws=smartwifi985698256ad256")
    suspend fun getExpenseCategory(@Body inputValidity: InputValidity): ResponseExpenseCategory

    @POST("list_expenses?ws=smartwifi985698256ad256")
    suspend fun getExpenseList(@Body inputValidity: InputReport): ResponseExpenseList

    @POST("add_expense?ws=smartwifi985698256ad256")
    suspend fun addExpense(@Body inputExpenseAdd: InputExpenseAdd): ResponseExpenseAdd

    @POST("add_location?ws=smartwifi985698256ad256")
    suspend fun updateLocation(@Body inputLocationUpdate: InputLocationUpdate): ResponseLocationUpdate

}