package com.azinova.preference

import android.content.Context
import android.content.SharedPreferences
import com.azinova.model.response.login.UserDetails
import com.google.gson.Gson

object SharedPrefs {

    private lateinit var sharedPreferences: SharedPreferences

    private const val PREF_NAME = "prefName"
    private const val USER_DETAILS = "userDetails"

    fun clearSession(context: Context) {
        val editor = context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.apply()
    }

    fun with(context: Context) {
        sharedPreferences = context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }


    var user_details: UserDetails?
        get() = Gson().fromJson(sharedPreferences.getString(USER_DETAILS, ""), UserDetails::class.java)
        set(id) = sharedPreferences.edit().putString(USER_DETAILS,Gson().toJson(id)).apply()




}