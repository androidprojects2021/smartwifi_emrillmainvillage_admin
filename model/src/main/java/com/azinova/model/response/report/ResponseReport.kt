package com.azinova.model.response.report

import com.google.gson.annotations.SerializedName

data class ResponseReport(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Data(

	@field:SerializedName("totalamount")
	val totalamount: String? = null,

	@field:SerializedName("coupons")
	val coupons: List<CouponsItem?>? = null,

	@field:SerializedName("totalsale")
	val totalsale: String? = null
)

data class CouponsItem(

	@field:SerializedName("sell_date")
	val sellDate: String? = null,

	@field:SerializedName("voucher_name")
	val voucherName: String? = null,

	@field:SerializedName("voucher_price")
	val voucherPrice: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("validity")
	val validity: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
