package com.azinova.model.response.dashboard

import com.google.gson.annotations.SerializedName

data class ResponseDashboard(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Sale(

	@field:SerializedName("total")
	val total: String? = null,

	@field:SerializedName("this_month")
	val thisMonth: String? = null,

	@field:SerializedName("today")
	val today: String? = null
)

data class IncomeData(

	@field:SerializedName("total")
	val total: String? = null,
	@field:SerializedName("cash_in_hand")
	val cash_in_hand: String? = null,
	@field:SerializedName("paid_amount")
	val paid_amount: String? = null,
	@field:SerializedName("todayincome")
	val todayincome: String? = null
)

data class Data(

	@field:SerializedName("sale")
	val sale: Sale? = null,

	@field:SerializedName("analysis")
	val analysis: List<Any?>? = null,

	@field:SerializedName("income_data")
	val incomeData: IncomeData? = null
)
