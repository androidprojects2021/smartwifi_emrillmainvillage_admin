package com.azinova.model.response.saleVoucher

import com.google.gson.annotations.SerializedName

data class ResponseSaleVoucher(

	@field:SerializedName("voucher_name")
	val voucherName: String? = null,

	@field:SerializedName("validity")
	val validity: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
