package com.azinova.model.response.expense_list

import com.google.gson.annotations.SerializedName

data class ResponseExpenseList(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class PaymentsItem(

	@field:SerializedName("addeddate")
	val addeddate: String? = null,

	@field:SerializedName("expense_amount")
	val expenseAmount: String? = null,

	@field:SerializedName("expense_category")
	val expenseCategory: String? = null,

	@field:SerializedName("expense_date")
	val expenseDate: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("supplier_name")
	val supplierName: String? = null,

	@field:SerializedName("expense_id")
	val expenseId: String? = null
)

data class Data(

	@field:SerializedName("totalamount")
	val totalamount: String? = null,

	@field:SerializedName("payments")
	val payments: List<PaymentsItem?>? = null
)
