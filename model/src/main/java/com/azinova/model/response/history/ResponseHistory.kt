package com.azinova.model.response.history

import com.google.gson.annotations.SerializedName

data class ResponseHistory(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataItem(

	@field:SerializedName("sell_date")
	val sellDate: String? = null,

	@field:SerializedName("voucher_name")
	val voucherName: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("validity")
	val validity: String? = null
)
