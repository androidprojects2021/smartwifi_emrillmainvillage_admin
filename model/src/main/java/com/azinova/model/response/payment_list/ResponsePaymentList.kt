package com.azinova.model.response.payment_list

import com.google.gson.annotations.SerializedName

data class ResponsePaymentList(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class PaymentsItem(

	@field:SerializedName("paidamount")
	val paidamount: String? = null,

	@field:SerializedName("datetime")
	val datetime: String? = null,

	@field:SerializedName("hotspot")
	val hotspot: String? = null,

	@field:SerializedName("user")
	val user: String? = null
)

data class Data(

	@field:SerializedName("totalamount")
	val totalamount: String? = null,

	@field:SerializedName("payments")
	val payments: List<PaymentsItem?>? = null
)
