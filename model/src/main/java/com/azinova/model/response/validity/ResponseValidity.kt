package com.azinova.model.response.validity

import com.google.gson.annotations.SerializedName

data class ResponseValidity(

	@field:SerializedName("profiles")
	val profiles: List<ProfilesItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class ProfilesItem(

	@field:SerializedName("validity_profile")
	val validityProfile: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)
