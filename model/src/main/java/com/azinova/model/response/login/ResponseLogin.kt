package com.azinova.model.response.login

import com.google.gson.annotations.SerializedName

data class ResponseLogin(

	@field:SerializedName("user_details")
	val userDetails: UserDetails? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class UserDetails(

	@field:SerializedName("hotspot_session_id")
	var hotspotSessionId: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("hotspot_session_name")
	var hotspotSessionName: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("hotspots")
	val hotspots : MutableList<Hotspots>? = null
)

data class Hotspots(

	@field:SerializedName("hotspot_session_id")
	val hotspotSessionId: String? = null,

	@field:SerializedName("hotspot_session_name")
	val hotspotSessionName: String? = null
)
