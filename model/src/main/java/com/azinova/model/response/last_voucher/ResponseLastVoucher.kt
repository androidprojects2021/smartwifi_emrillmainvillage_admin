package com.azinova.model.response.last_voucher

import com.google.gson.annotations.SerializedName

data class ResponseLastVoucher(

	@field:SerializedName("voucher_detail")
	val voucherDetail: VoucherDetail? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class VoucherDetail(

	@field:SerializedName("sell_date")
	val sellDate: String? = null,

	@field:SerializedName("voucher_validity")
	val voucherValidity: String? = null,

	@field:SerializedName("voucher_name")
	val voucherName: String? = null,

	@field:SerializedName("voucher_id")
	val voucherId: String? = null
)
