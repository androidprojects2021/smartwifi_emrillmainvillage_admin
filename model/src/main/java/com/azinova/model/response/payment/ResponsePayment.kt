package com.azinova.model.response.payment

import com.google.gson.annotations.SerializedName

data class ResponsePayment(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
