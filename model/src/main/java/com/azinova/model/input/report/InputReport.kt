package com.azinova.model.input.report

import com.google.gson.annotations.SerializedName

data class InputReport(

	@field:SerializedName("hotspot_session_id")
	val hotspotSessionId: String? = null,

	@field:SerializedName("from_date")
	val fromDate: String? = null,

	@field:SerializedName("to_date")
	val toDate: String? = null,

	@field:SerializedName("hotspot_session_name")
	val hotspotSessionName: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("token")
	val token: String? = null
)
