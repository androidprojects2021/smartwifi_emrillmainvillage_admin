package com.azinova.model.input.validity

import com.google.gson.annotations.SerializedName

data class InputValidity(

	@field:SerializedName("hotspot_session_id")
	val hotspotSessionId: String? = null,

	@field:SerializedName("hotspot_session_name")
	val hotspotSessionName: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("token")
	val token: String? = null
)
