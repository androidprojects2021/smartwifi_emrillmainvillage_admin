package com.azinova.model.input.payment

import com.google.gson.annotations.SerializedName

data class InputPay(

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("hotspot_session_id")
	val hotspotSessionId: String? = null,

	@field:SerializedName("session_user_id")
	val sessionUserId: String? = null,

	@field:SerializedName("hotspot_session_name")
	val hotspotSessionName: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("collected_date")
	val collected_date: String? = null,

	@field:SerializedName("description")
	val description:String? = null


)
