package com.azinova.model.input.location_update

import com.google.gson.annotations.SerializedName

data class InputLocationUpdate(

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("latitude")
	val latitude: String? = null,

	@field:SerializedName("longitude")
	val longitude: String? = null
)
