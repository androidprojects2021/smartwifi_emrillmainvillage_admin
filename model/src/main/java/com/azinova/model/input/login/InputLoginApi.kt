package com.azinova.model.input.login

import com.google.gson.annotations.SerializedName


data class InputLoginApi(

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)
