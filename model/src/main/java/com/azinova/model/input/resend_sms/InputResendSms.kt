package com.azinova.model.input.resend_sms

import com.google.gson.annotations.SerializedName

data class InputResendSms(

	@field:SerializedName("hotspot_session_id")
	val hotspotSessionId: String? = null,

	@field:SerializedName("voucher_name")
	val voucherName: String? = null,

	@field:SerializedName("hotspot_session_name")
	val hotspotSessionName: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("validity")
	val validity: String? = null,

	@field:SerializedName("token")
	val token: String? = null
)
