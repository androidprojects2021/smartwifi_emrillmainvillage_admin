package com.azinova.model.input.expense_add

import com.google.gson.annotations.SerializedName

data class InputExpenseAdd(

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("hotspot_session_id")
	val hotspotSessionId: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("hotspot_session_name")
	val hotspotSessionName: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("supplier")
	val supplier: String? = null,

	@field:SerializedName("expense_date")
	val expenseDate: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("token")
	val token: String? = null
)
