package com.azinova.smartwifiadminemv.login

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.commons.ExitAppDialog
import com.azinova.commons.Loader
import com.azinova.model.input.login.InputLoginApi
import com.azinova.smartwifiadminemv.App
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.home.HomeActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), ExitAppDialog.ExitClick  {

    lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        App().sharefPrefs.user_details=null
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        onClick()
        onObserve()
    }

    private fun onObserve() {
        viewModel.loginResponse.observe(this, Observer {

            Loader.hideLoader()

            if(it.status.equals("success")) {
                viewModel.loginResponseClear()
                val intent =Intent(this,HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent).also { viewModel.loginResponseClear() } }
            else { Snackbar.make(outer, it.message.toString(), Snackbar.LENGTH_SHORT).show()}
        })
    }

    private fun onClick() {
        loginButton.setOnClickListener {
            if(isValidLogin() ){
                Loader.showLoader(this)
                viewModel.loginApiCall(InputLoginApi(password.text.toString(),username.text.toString()))}
        }
    }

    private fun isValidLogin(): Boolean {

        if (ActivityCompat.checkSelfPermission(
                applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)  {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 101)
                return false
        }

        val locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            Toast.makeText(this, "Turn on GPS", Toast.LENGTH_SHORT).show()
            return false
        }


        if(username.text.isNullOrBlank()){ Snackbar.make(outer, "please enter username", Snackbar.LENGTH_SHORT).show()
        return false}

        if(password.text.isNullOrBlank()){ Snackbar.make(outer, "please enter password", Snackbar.LENGTH_SHORT).show()
        return false}

        return true
    }

    override fun onBackPressed() {
        ExitAppDialog.showDialog(this,this)
    }

    override fun proceedLogout() {
        super.onBackPressed()
    }

}