package com.azinova.smartwifiadminemv.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.login.InputLoginApi
import com.azinova.model.response.login.ResponseLogin
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class LoginViewModel: ViewModel() {


    private val _loginResponse = MutableLiveData<ResponseLogin>()
    val loginResponse : LiveData<ResponseLogin>
    get() = _loginResponse

    fun loginResponseClear(){

        _loginResponse.value = ResponseLogin(status = "idle")

    }

    fun loginApiCall(inputLoginApi: InputLoginApi) {

        viewModelScope.launch {
            try {

                val response =  App().retrofit.login(inputLoginApi)
                if(response.status.equals("success")){
                    App().sharefPrefs.user_details = response.userDetails
                    Log.e("UserDetails",response.userDetails.toString())
                }
                _loginResponse.value = response

            } catch (e: Exception) {
                  _loginResponse.value = ResponseLogin(message = "Something went wrong" ,status = "Error")
            }
        }

    }

}