package com.azinova.smartwifiadminemv

import android.app.Application
import com.azinova.constants.NetworkConstants
import com.azinova.network.RetrofitClient
import com.azinova.preference.SharedPrefs

class App:Application() {

    val sharefPrefs = SharedPrefs
    val retrofit = RetrofitClient.getRetrofit(NetworkConstants.BaseUrl)

    override fun onCreate() {
        super.onCreate()

        SharedPrefs.with(applicationContext)

    }

}