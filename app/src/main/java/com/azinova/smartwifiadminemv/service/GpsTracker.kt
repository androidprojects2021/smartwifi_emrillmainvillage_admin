package com.azinova.smartwifiadminemv.service

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.IBinder
import android.util.Log
import androidx.core.app.ActivityCompat
import com.azinova.model.input.location_update.InputLocationUpdate
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class GpsTracker: Service() {

    var status = true

    override fun onBind(p0: Intent?): IBinder? {

        return null

    }

    override fun onStart(intent: Intent?, startId: Int) {
        super.onStart(intent, startId)
        Log.e("Lon","Start")
        status =true

        GlobalScope.launch {
            getLocation()
            while (status){
                delay(300000)
                getLocation()
            }


        }

        Log.e("Loc Start asda" , "p0.latitude.toString(()")

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)

        Log.e("Loc Start" , "p0.latitude.toString(()")

    }




    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(
                applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)  {
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val locationGPS: Location? = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (locationGPS != null) {
                val lat = locationGPS.latitude.toString()
                val longi = locationGPS.longitude.toString()


                GlobalScope.launch {

                    val response = App().retrofit.updateLocation(InputLocationUpdate(App().sharefPrefs.user_details?.userId,lat,longi))

                }

                Log.e("Lat",lat.toString())
                Log.e("Lon",longi.toString())

            } else {

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        status = false
        Log.e("Lon","Destroy")
    }

}