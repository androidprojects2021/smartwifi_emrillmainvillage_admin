package com.azinova.smartwifiadminemv.dashboard

import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.commons.Loader
import com.azinova.model.input.dashboard.InputDashboard
import com.azinova.model.response.login.Hotspots
import com.azinova.model.response.login.UserDetails
import com.azinova.smartwifiadminemv.App
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.dashboard.change_camp.ChangeCampDialog
import com.azinova.smartwifiadminemv.home.HomeActivity
import com.azinova.smartwifiadminemv.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.dash_board_fragment.*
import kotlinx.android.synthetic.main.dashboard_card_bottom.*

class DashBoardFragment : Fragment(),ChangeCampDialog.ClickCampsChange {

    private lateinit var viewModel: DashBoardViewModel
    private lateinit var parentActivity : HomeActivity


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dash_board_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(DashBoardViewModel::class.java)
        parentActivity = activity as HomeActivity

        setUpDash()
        observe()
        clicks()

    }

    override fun onResume() {
        super.onResume()
        val locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            Toast.makeText(requireContext(), "Turn on GPS", Toast.LENGTH_SHORT).show()
            parentActivity.OnGPS()
        }
    }

    private fun clicks() {
        parentActivity.changeCampLayout.setOnClickListener {
            val changeCampDialog = ChangeCampDialog(requireContext(),this)
            changeCampDialog.showPopup()
        }
    }

    private fun observe() {
        viewModel.dashBoardResponse.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if(it.status.equals("success")){
                thisMonth.text = "${it.data?.sale?.thisMonth ?: "0"} Nos"
                today.text = "${it.data?.sale?.today ?: "0"} Nos"
                total.text ="${ it.data?.sale?.total ?: "0"} Nos"
                totalIncome.text = "${it.data?.incomeData?.todayincome ?: "0"}"
                cashInHand.text = "${it.data?.incomeData?.total ?: "0"}"
                paid1.text = "${it.data?.incomeData?.paid_amount ?: "0"}"
                paid2.text = "${it.data?.incomeData?.cash_in_hand ?: "0"}"
                viewModel.clearResponse()
            }else if(it.status.equals("token_error")){
                val intent =Intent(requireContext(),LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }else if(!it.status.equals("idle")){
                Snackbar.make(dashOuter, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })
    }

    private fun setUpDash() {

        parentActivity.toolbarHeaderText.text = "${App().sharefPrefs.user_details?.hotspotSessionName} - ${App().sharefPrefs.user_details?.name}"

        try {
            Loader.showLoader(requireContext())
        } catch (e: Exception) {
        }
        viewModel.getDashboard(InputDashboard(App().sharefPrefs.user_details?.hotspotSessionId,
            App().sharefPrefs.user_details?.hotspotSessionName,
            App().sharefPrefs.user_details?.userId,
            App().sharefPrefs.user_details?.token))
    }


    override fun changeCamp(hotspots: Hotspots) {
        var userDetails: UserDetails? = App().sharefPrefs.user_details
        userDetails?.hotspotSessionName = hotspots.hotspotSessionName
        userDetails?.hotspotSessionId = hotspots.hotspotSessionId
        App().sharefPrefs.user_details = userDetails
        setUpDash()
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}