package com.azinova.smartwifiadminemv.dashboard.change_camp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.login.Hotspots
import com.azinova.smartwifiadminemv.App
import com.azinova.smartwifiadminemv.R

class ChangeCampDialog(val context: Context, val clickCampsChange: ClickCampsChange) :
    ChangeCampAdapter.ClickHotspotListAdapter {


    private lateinit var builder: AlertDialog.Builder
    private lateinit var customLayout: View
    private lateinit var dialog: AlertDialog
    private lateinit var adapter: ChangeCampAdapter

    var HOTSPOT_SELECTED :Hotspots? = null

    fun showPopup() {

        try {
            builder = AlertDialog.Builder(context)
            customLayout = LayoutInflater.from(context).inflate(R.layout.change_camp_dialog, null)
            builder.setView(customLayout)
            dialog = builder.create()
            dialog.setCancelable(true)

            dialog.show()

            setData()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setData() {

        val recyclerView = customLayout.findViewById<RecyclerView>(R.id.recyclerView)
        val close = customLayout.findViewById<TextView>(R.id.closePopup)
        val proceedButton = customLayout.findViewById<TextView>(R.id.proceedCampChangeButton)

        var campList: MutableList<Hotspots> = App().sharefPrefs.user_details?.hotspots ?: mutableListOf()
        adapter = ChangeCampAdapter(context, campList, this)
        recyclerView.layoutManager = GridLayoutManager(context, 1)
        recyclerView.adapter=adapter


        close.setOnClickListener { hidePopup() }
        proceedButton.setOnClickListener {

            if(HOTSPOT_SELECTED == null){

            }else{
                clickCampsChange.changeCamp(HOTSPOT_SELECTED!!)
                hidePopup()
            }

        }


    }


    fun hidePopup() {
        try {
            dialog.dismiss()
        } catch (e: Exception) {
        }
    }


    interface ClickCampsChange {
        fun changeCamp(hotspots: Hotspots)
    }

    override fun selectedHotspot(hotspots: Hotspots) {
        HOTSPOT_SELECTED = hotspots
    }
}