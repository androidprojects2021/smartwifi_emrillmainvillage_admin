package com.azinova.smartwifiadminemv.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.dashboard.InputDashboard
import com.azinova.model.response.dashboard.ResponseDashboard
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class DashBoardViewModel : ViewModel() {


    private val _dashBoardResponse = MutableLiveData<ResponseDashboard>()
    val dashBoardResponse: LiveData<ResponseDashboard>
        get() = _dashBoardResponse

    fun clearResponse() {
        _dashBoardResponse.value = ResponseDashboard(status = "idle")
    }


    fun getDashboard(inputDashboard: InputDashboard) {
        viewModelScope.launch {
            try {

                val response = App().retrofit.dashBoard(inputDashboard)
                _dashBoardResponse.value = response

            } catch (e: Exception) {

                _dashBoardResponse.value =
                    ResponseDashboard(message = "Something went wrong", status = "Error")
            }
        }

    }


}