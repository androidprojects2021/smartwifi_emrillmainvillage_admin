package com.azinova.smartwifiadminemv.sale.sales_confirm

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.azinova.model.input.saleVoucher.InputSaleVoucher
import com.azinova.model.response.last_voucher.VoucherDetail
import com.azinova.smartwifiadminemv.R

class SaleConfirmPopup(
    val context: Context,
    val confirmPopupClick: ConfirmPopupClick,
    val voucherDetail: VoucherDetail?,
    val inputSaleVoucher: InputSaleVoucher
) {

    private lateinit var builder: AlertDialog.Builder
    private lateinit var customLayout: View
    private lateinit var dialog: AlertDialog

    fun showPopup() {
        try {
            builder = AlertDialog.Builder(context)
            customLayout = LayoutInflater.from(context).inflate(R.layout.popup_confirm_sale, null)
            builder.setView(customLayout)
            dialog = builder.create()
            dialog.setCancelable(false)

            setData(voucherDetail, inputSaleVoucher)

            dialog.show()
        } catch (e: Exception) {
        }
    }


    private fun setData(data: VoucherDetail?, inputSaleVoucher: InputSaleVoucher) {
        val confirmPhone = customLayout.findViewById<TextView>(R.id.confirmPhoneNumber)
        val confirmValidity = customLayout.findViewById<TextView>(R.id.confirmValidity)
        val prevName = customLayout.findViewById<TextView>(R.id.prevName)
        val prevDate = customLayout.findViewById<TextView>(R.id.prevDate)
        val prevValidity = customLayout.findViewById<TextView>(R.id.prevValidity)
        val proceedButton = customLayout.findViewById<Button>(R.id.proceedButton)
        val cancelButton = customLayout.findViewById<Button>(R.id.cancelButton)

        confirmPhone.text = inputSaleVoucher.mobile
        confirmValidity.text = inputSaleVoucher.validity
        prevName.text = data?.voucherName
        prevDate.text = data?.sellDate
        prevValidity.text = data?.voucherValidity

        proceedButton.setOnClickListener {
            confirmPopupClick.clickOkButton()
            hidePopup()
        }
        cancelButton.setOnClickListener { hidePopup() }

    }

    fun hidePopup() {
        try {
            dialog.dismiss()
        } catch (e: Exception) {
        }
    }


}

