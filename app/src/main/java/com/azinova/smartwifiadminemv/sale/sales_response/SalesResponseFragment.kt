package com.azinova.smartwifiadminemv.sale.sales_response

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.azinova.commons.Loader
import com.azinova.smartwifiadminemv.R
import kotlinx.android.synthetic.main.sales_response_fragment.*

class SalesResponseFragment : Fragment() {


    private lateinit var viewModel: SalesResponseViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sales_response_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(SalesResponseViewModel::class.java)

        val arguments = requireArguments()
        cpCode.setText(arguments.getString("coupon_number"))
        validityValue.setText(arguments.getString("validity"))
        onClick()


    }

    private fun onClick() {
        home.setOnClickListener {
            findNavController().navigate(R.id.action_salesResponseFragment_to_dashBoardFragment)
        }
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }
}