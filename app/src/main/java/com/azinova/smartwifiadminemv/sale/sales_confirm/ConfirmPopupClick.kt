package com.azinova.smartwifiadminemv.sale.sales_confirm

interface ConfirmPopupClick {
    fun clickOkButton()
}