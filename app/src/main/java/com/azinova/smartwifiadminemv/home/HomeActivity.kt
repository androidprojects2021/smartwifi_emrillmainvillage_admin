package com.azinova.smartwifiadminemv.home

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.azinova.commons.ExitAppDialog
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.service.GpsTracker
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity() , ExitAppDialog.ExitClick{

    lateinit var toolbarHeaderText:TextView
    lateinit var  navController : NavController
    lateinit var changeCampLayout: ConstraintLayout
    lateinit var editIconImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        bottomNavigationView.setupWithNavController(findNavController(R.id.fragment))
        navController=findNavController(R.id.fragment)

        changeCampLayout = changeCamp
        editIconImage = editIcon
        toolbarHeaderText = toolBarHeader


       /* val nManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!nManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            OnGPS()
        }*/


        serviceGps()

        onDestination()

    }

    override fun onResume() {
        super.onResume()
        val locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            Toast.makeText(this, "Turn on GPS", Toast.LENGTH_SHORT).show()
            OnGPS()
        }
    }

    fun OnGPS() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes",
                DialogInterface.OnClickListener { dialog, which -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                dialog.dismiss()

                })

        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()

    }

    private fun serviceGps() {

        startService(Intent(this, GpsTracker::class.java ))

        Log.e("serv ","start")

    }

    private fun onDestination() {
        findNavController(R.id.fragment).addOnDestinationChangedListener { controller, destination, arguments ->
            when(destination.id)
            {
                R.id.salesResponseFragment  -> {
                    bottomNavigationView?.visibility=View.GONE
                    toolbar.visibility=View.GONE
                    changeCampLayout.setOnClickListener {  }}

                R.id.dashBoardFragment -> {
                    bottomNavigationView?.visibility = View.VISIBLE
                    editIconImage.visibility = View.VISIBLE
                    toolbar.visibility=View.VISIBLE}

                R.id.historyFragment ,R.id.reportFragment
                    ,R.id.expenseAddFragment,R.id.expenseListFragment,
                R.id.paymentListFragment -> {
                    changeCampLayout.setOnClickListener {  }
                    bottomNavigationView?.visibility=View.GONE
                    toolbar.visibility=View.GONE }

                else ->{
                    toolbar.visibility=View.VISIBLE
                    changeCampLayout.setOnClickListener {  }
                    bottomNavigationView?.visibility = View.VISIBLE
                    editIconImage.visibility = View.INVISIBLE}
            }
        }
    }


    override fun onBackPressed() {
        when(navController.currentDestination?.id){
            R.id.dashBoardFragment->{ExitAppDialog.showDialog(this,this)}
            else->{super.onBackPressed()}
        }
    }

    override fun proceedLogout() {
        super.onBackPressed()
    }


    fun stopServiceTracker(){
        stopService(Intent(this, GpsTracker::class.java))
    }

}