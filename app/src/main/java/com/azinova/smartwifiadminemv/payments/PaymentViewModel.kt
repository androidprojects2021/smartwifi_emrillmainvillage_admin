package com.azinova.smartwifiadminemv.payments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.payment.InputPay
import com.azinova.model.input.users.InputGetUser
import com.azinova.model.response.payment.ResponsePayment
import com.azinova.model.response.users.ResponseGetUser
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class PaymentViewModel : ViewModel() {

    private val _userResponse = MutableLiveData<ResponseGetUser>()
    val userResponse : LiveData<ResponseGetUser>
    get() = _userResponse

    private val _paymentResponse = MutableLiveData<ResponsePayment>()
    val paymentResponse : LiveData<ResponsePayment>
        get() = _paymentResponse


    fun clearPaymentResponse(){
        _paymentResponse.value = ResponsePayment(status = "idle")
    }



    fun clearUserResponse(){
        _userResponse.value = ResponseGetUser(status = "idle")
    }

    fun getUsers(inputGetUser: InputGetUser){
        viewModelScope.launch {
            try {

                val response = App().retrofit.getUser(inputGetUser)
                _userResponse.value = response

            } catch (e: Exception) {
                _userResponse.value = ResponseGetUser(message = "Something went wrong" ,status = "Error")
            }
        }
    }

    fun paymentCall(inputPay: InputPay){
        viewModelScope.launch {
            try {

                val response = App().retrofit.pay(inputPay)
                _paymentResponse.value = response

            } catch (e: Exception) {
                _paymentResponse.value = ResponsePayment(message = "Something went wrong" ,status = "Error")
            }
        }
    }

}