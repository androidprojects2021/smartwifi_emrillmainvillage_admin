package com.azinova.smartwifiadminemv.payments.list_payments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.payment_list.PaymentsItem
import com.azinova.model.response.report.CouponsItem
import com.azinova.smartwifiadminemv.R
import kotlinx.android.synthetic.main.adapter_pay_list.view.*

class AdapterPayList (val context: Context): RecyclerView.Adapter<AdapterPayList.ViewHolder>() {

    var data : List<PaymentsItem?>? = listOf()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val user =itemView.userText
        val hotspot =itemView.hotspotText
        val date =itemView.dateText
        val amount =itemView.amount

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPayList.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_pay_list,parent,false))
    }

    override fun getItemCount(): Int {
        return data?.size ?:0
    }

    override fun onBindViewHolder(holder: AdapterPayList.ViewHolder, position: Int) {

        holder.user.text = data?.get(position)?.user ?:""
        holder.hotspot.text = data?.get(position)?.hotspot ?:""
        holder.date.text = data?.get(position)?.datetime ?:""
        holder.amount.text = "AED "+ data?.get(position)?.paidamount ?:""

    }


    fun refreshData(data : List<PaymentsItem?>?){
        this.data = data
        notifyDataSetChanged()
    }


    interface ClickReport{
        fun reportSelected(item : CouponsItem)
    }

}