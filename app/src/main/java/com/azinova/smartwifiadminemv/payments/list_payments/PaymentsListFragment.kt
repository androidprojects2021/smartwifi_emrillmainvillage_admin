package com.azinova.smartwifiadminemv.payments.list_payments

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.commons.Loader
import com.azinova.model.input.report.InputReport
import com.azinova.model.response.payment_list.PaymentsItem
import com.azinova.smartwifiadminemv.App
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.login.LoginActivity
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.payments_list_fragment.*
import kotlinx.android.synthetic.main.payments_list_fragment.closeSearch
import kotlinx.android.synthetic.main.payments_list_fragment.constraintLayout3
import kotlinx.android.synthetic.main.payments_list_fragment.dateSelect
import kotlinx.android.synthetic.main.payments_list_fragment.fromDate
import kotlinx.android.synthetic.main.payments_list_fragment.recyclerReport
import kotlinx.android.synthetic.main.payments_list_fragment.searchLayout
import kotlinx.android.synthetic.main.payments_list_fragment.searchPhoneText
import kotlinx.android.synthetic.main.payments_list_fragment.searchReport
import kotlinx.android.synthetic.main.payments_list_fragment.toDate
import java.text.SimpleDateFormat
import java.util.*

class PaymentsListFragment : Fragment() {

    val builder= MaterialDatePicker.Builder.dateRangePicker()
    val picker = builder.build()
    private lateinit var adapter: AdapterPayList
    private lateinit var viewModel: PaymentsListViewModel

    var data : List<PaymentsItem?>? = listOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.payments_list_fragment, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PaymentsListViewModel::class.java)
        onClick()
        onObserve()

        adapter = AdapterPayList(requireContext())
        recyclerReport.layoutManager= LinearLayoutManager(requireContext())
        recyclerReport.adapter = adapter

        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf.format(Date())
        fromDate.text = currentDate
        toDate.text = currentDate

        picker.addOnPositiveButtonClickListener {

            Loader.showLoader(requireContext())

            fromDate.text = convertLongToTime(it.first!!)
            toDate.text = convertLongToTime(it.second!!)

            viewModel.getPayList(InputReport(
                App().sharefPrefs.user_details?.hotspotSessionId,
                        convertLongToTime(it.first!!),
                convertLongToTime(it.second!!),
                App().sharefPrefs.user_details?.hotspotSessionName,
                App().sharefPrefs.user_details?.userId,
                App().sharefPrefs.user_details?.token
            ))
        }


        viewModel.getPayList(InputReport(
            App().sharefPrefs.user_details?.hotspotSessionId,
            fromDate.text.toString(),
            toDate.text.toString(),
            App().sharefPrefs.user_details?.hotspotSessionName,
            App().sharefPrefs.user_details?.userId,
            App().sharefPrefs.user_details?.token
        ))

    }

    private fun onObserve() {
        viewModel.payList .observe(viewLifecycleOwner, androidx.lifecycle.Observer {

            Loader.hideLoader()

            if(it.status.equals("success")) {
                viewModel.payListResponseClear()

                totalValue.text="AED "+it.data?.totalamount.toString()

                if(it.data?.payments?.size!=0){
                    searchReport.visibility=View.VISIBLE
                    recyclerReport.visibility= View.VISIBLE
                    noDataReport.visibility= View.GONE
                    data  = it.data?.payments
                    adapter.refreshData(it.data?.payments)

                }else{
                    recyclerReport.visibility= View.GONE
                    noDataReport.visibility= View.VISIBLE
                }


            }else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerPayList, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })
    }

    private fun onClick() {
        dateSelect.setOnClickListener {


            try {
                picker.show(parentFragmentManager, picker.toString())
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

        searchReport.setOnClickListener {
            searchReport.visibility=View.GONE
            searchLayout.visibility=View.VISIBLE
            constraintLayout3.visibility=View.INVISIBLE

        }


        closeSearch.setOnClickListener {
            searchReport.visibility=View.VISIBLE
            searchLayout.visibility=View.GONE
            constraintLayout3.visibility=View.VISIBLE
            searchPhoneText.text.clear()
            adapter.refreshData(data)
        }


        searchPhoneText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(!p0.toString().equals("")){

                    val searched = mutableListOf<PaymentsItem?>()

                    for (items in data!!){
                        if(items?.user?.contains(p0.toString(),true)!!){
                            searched.add(items)
                        }
                        adapter.refreshData(searched)
                    }

                }else{
                    adapter.refreshData(data)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

    }

    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.format(date)
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}