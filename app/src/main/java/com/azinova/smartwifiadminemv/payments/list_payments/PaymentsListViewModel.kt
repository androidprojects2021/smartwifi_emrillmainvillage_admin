package com.azinova.smartwifiadminemv.payments.list_payments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.report.InputReport
import com.azinova.model.response.payment_list.ResponsePaymentList
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class PaymentsListViewModel : ViewModel() {


    private val _payList = MutableLiveData<ResponsePaymentList>()
    val payList : LiveData<ResponsePaymentList>
    get() = _payList



    fun payListResponseClear(){
        _payList.value = ResponsePaymentList(status = "idle")
    }

    fun getPayList(inputReport: InputReport){

        viewModelScope.launch {
            try {

                val response = App().retrofit.getListPayments(inputReport)

                _payList.value = response

            } catch (e: Exception) {
                _payList.value = ResponsePaymentList(message = "Something went wrong" ,status = "Error")
            }
        }

    }




}