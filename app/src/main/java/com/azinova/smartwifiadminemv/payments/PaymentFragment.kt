package com.azinova.smartwifiadminemv.payments

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.commons.Loader
import com.azinova.model.input.payment.InputPay
import com.azinova.model.input.users.InputGetUser
import com.azinova.model.response.users.UsersItem
import com.azinova.smartwifiadminemv.App
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.payment_fragment.*
import kotlinx.android.synthetic.main.payment_fragment.datePicked
import kotlinx.android.synthetic.main.payment_fragment.dateSelector
import java.util.*

class PaymentFragment : Fragment() {

    private lateinit var viewModel: PaymentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.payment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(PaymentViewModel::class.java)

        getUser()
        onClick()
        observe()

    }

    private fun observe() {

        viewModel.userResponse.observe(viewLifecycleOwner, Observer {

            Loader.hideLoader()
            if(it.status.equals("success")){

                if(it.users?.size!! >0) {
                    val adapter = UserSpinnerAdapter(requireContext(),R.layout.custom_spinner, it?.users ?: listOf() )
                    userSpinner.adapter=adapter

                }else {
                    Snackbar.make(payOuter, "No user available", Snackbar.LENGTH_SHORT).show()
                }
                viewModel.clearUserResponse()
            }
            else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(payOuter, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })


        viewModel.paymentResponse.observe(viewLifecycleOwner, Observer {

            Loader.hideLoader()
            if(it.status.equals("success")){

                    amount.text.clear()
                datePicked.text="Select Date"
                descriptionPayment.text.clear()


                    Toast.makeText(requireContext(),it.message,Toast.LENGTH_SHORT)
                    viewModel.clearUserResponse()

            }
            else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(payOuter, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })

    }

    private fun onClick() {

        dateSelector.setOnClickListener {
            val cldr: Calendar = Calendar.getInstance()
            val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
            val month: Int = cldr.get(Calendar.MONTH)
            val year: Int = cldr.get(Calendar.YEAR)
            val datepicker = DatePickerDialog(requireContext(),
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    datePicked.text =
                        "$year-${if ((monthOfYear + 1) < 10) "0" + (monthOfYear + 1) else (monthOfYear + 1)}-${if (dayOfMonth < 10) "0" + dayOfMonth else dayOfMonth}"
                    datePicked.setTextColor(Color.BLACK)

                }, year, month, day
            )
            datepicker.show()
        }

        submitButtonPay.setOnClickListener {
            if (valid()) {
                Loader.showLoader(requireContext())
                val user :UsersItem? = userSpinner.selectedItem as UsersItem?
                viewModel.paymentCall(InputPay(amount.text.toString(),
                    App().sharefPrefs.user_details?.hotspotSessionId,
                    user?.userId,
                    App().sharefPrefs.user_details?.hotspotSessionName,
                    App().sharefPrefs.user_details?.userId,
                    App().sharefPrefs.user_details?.token,
                    datePicked.text.toString(),descriptionPayment.text.toString()
                ))
            }
        }

    }

    private fun valid(): Boolean {

        val user :UsersItem? = userSpinner.selectedItem as UsersItem?

        if(user == null){
            Snackbar.make(payOuter, "No users available", Snackbar.LENGTH_SHORT).show()
            return false
        }

        if(amount.text.toString().isNullOrBlank()){
            Snackbar.make(payOuter, "Please enter valid amount", Snackbar.LENGTH_SHORT).show()
            return false
        }

        if (datePicked.text.toString().equals("Select Date")){
            Snackbar.make(payOuter, "Please Select Date", Snackbar.LENGTH_SHORT).show()
            return false
        }

        return true

    }

    private fun getUser() {
        Loader.showLoader(requireContext())
        viewModel.getUsers(InputGetUser(App().sharefPrefs.user_details?.hotspotSessionId,
            App().sharefPrefs.user_details?.hotspotSessionName,
            App().sharefPrefs.user_details?.userId,
            App().sharefPrefs.user_details?.token))
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}