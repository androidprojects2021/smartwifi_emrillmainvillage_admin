package com.azinova.smartwifiadminemv.report

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.report.CouponsItem
import com.azinova.smartwifiadminemv.R
import kotlinx.android.synthetic.main.adapter_report.view.*

class ReportAdapter(val context: Context):RecyclerView.Adapter<ReportAdapter.ViewHolder>() {

    var data : List<CouponsItem?>? = listOf()

    inner class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){

        val couponReportNo =itemView.couponReportNo
        val couponDateTimeRepo =itemView.couponDateTimeRepo

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_report,parent,false))
    }

    override fun getItemCount(): Int {
       return data?.size ?:0
    }

    override fun onBindViewHolder(holder: ReportAdapter.ViewHolder, position: Int) {

        holder.couponDateTimeRepo.text = data?.get(position)?.mobile ?:""
        holder.couponReportNo.text = data?.get(position)?.sellDate ?:""

    }


    fun refreshData(data : List<CouponsItem?>?){
        this.data = data
        notifyDataSetChanged()
    }


    interface ClickReport{
        fun reportSelected(item :CouponsItem)
    }

}