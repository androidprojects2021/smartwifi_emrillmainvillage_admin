package com.azinova.smartwifiadminemv.report

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.commons.Loader
import com.azinova.model.input.report.InputReport
import com.azinova.model.response.report.CouponsItem
import com.azinova.smartwifiadminemv.App
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.login.LoginActivity
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.report_fragment.*
import java.text.SimpleDateFormat
import java.util.*

class ReportFragment : Fragment() {

    private lateinit var viewModel: ReportViewModel
    private lateinit var adapter: ReportAdapter
    var data : List<CouponsItem?>? = listOf()

    val builder= MaterialDatePicker.Builder.dateRangePicker()
    val picker = builder.build()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.report_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ReportViewModel::class.java)


        adapter = ReportAdapter(requireContext())
        recyclerReport.layoutManager= LinearLayoutManager(requireContext())
        recyclerReport.adapter = adapter



        onClick()
        observe()
        picker.addOnPositiveButtonClickListener {
            fromDate.text = convertLongToTime(it.first!!)
            toDate.text = convertLongToTime(it.second!!)
            callApiResult(convertLongToTime(it.first!!),convertLongToTime(it.second!!))

        }

    }


    private fun observe() {

        viewModel.reportResponse.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

            Loader.hideLoader()

            if(it.status.equals("success")) {
                viewModel.reportResponseClear()
                countValue.setText(it?.data?.totalsale ?: "0")
                totalValue.setText(it?.data?.totalamount ?: "0")

                if(it.data?.coupons?.size!=0){
                    searchReport.visibility=View.VISIBLE
                    recyclerReport.visibility= View.VISIBLE
                    noDataReport.visibility= View.GONE
                    data  = it.data?.coupons
                    adapter.refreshData(it.data?.coupons)

                }else{
                    recyclerReport.visibility= View.GONE
                    noDataReport.visibility= View.VISIBLE
                }


            }else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerReport, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })



    }

    private fun onClick() {
        dateSelect.setOnClickListener {


            try {
                picker.show(parentFragmentManager, picker.toString())
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

        searchReport.setOnClickListener {
            searchReport.visibility=View.GONE
            searchLayout.visibility=View.VISIBLE
            constraintLayout3.visibility=View.INVISIBLE

        }


        closeSearch.setOnClickListener {
            searchReport.visibility=View.VISIBLE
            searchLayout.visibility=View.GONE
            constraintLayout3.visibility=View.VISIBLE
            searchPhoneText.text.clear()
            adapter.refreshData(data)
        }


        searchPhoneText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(!p0.toString().equals("")){

                    val searched = mutableListOf<CouponsItem?>()

                    for (items in data!!){
                        if(items?.mobile?.contains(p0.toString())!!){
                            searched.add(items)
                        }

                        adapter.refreshData(searched)
                    }

                }else{
                    adapter.refreshData(data)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

    }


    fun callApiResult(fromTime: String, toTime: String) {
        Loader.showLoader(requireContext())
        viewModel.reportApiCall(
            InputReport(App().sharefPrefs.user_details?.hotspotSessionId,
            fromTime,
            toTime,
            App().sharefPrefs.user_details?.hotspotSessionName,
            App().sharefPrefs.user_details?.userId,
            App().sharefPrefs.user_details?.token)
        )
    }


    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.format(date)
    }


    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}