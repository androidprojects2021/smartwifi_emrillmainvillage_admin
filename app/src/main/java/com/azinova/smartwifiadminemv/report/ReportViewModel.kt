package com.azinova.smartwifiadminemv.report

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.report.InputReport
import com.azinova.model.response.report.ResponseReport
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class ReportViewModel : ViewModel() {

    private val _reportResponse = MutableLiveData<ResponseReport>()
    val reportResponse : LiveData<ResponseReport>
        get() = _reportResponse


    fun reportResponseClear(){
        _reportResponse.value = ResponseReport(status = "idle")
    }


    fun reportApiCall(inputReport: InputReport){

        viewModelScope.launch {
            try {
                val response =  App().retrofit.salesReport(inputReport)
                _reportResponse.value = response

            } catch (e: Exception) {
                _reportResponse.value = ResponseReport(message = "Something went wrong" ,status = "Error")
            }
        }

    }


}