package com.azinova.smartwifiadminemv.more

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.azinova.commons.Loader
import com.azinova.commons.LogoutConfirmDialog
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.home.HomeActivity
import com.azinova.smartwifiadminemv.login.LoginActivity
import kotlinx.android.synthetic.main.more_fragment.*

class MoreFragment : Fragment() ,LogoutConfirmDialog.LogoutClick{


    private lateinit var viewModel: MoreViewModel
    private lateinit var parent : HomeActivity


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.more_fragment, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MoreViewModel::class.java)
        parent = activity as HomeActivity

        onClick()
    }


    private fun onClick() {
        logout.setOnClickListener {
            LogoutConfirmDialog.showDialog(requireContext(),this)

        }
        report.setOnClickListener { findNavController().navigate(R.id.action_moreFragment_to_reportFragment) }
        expenseList.setOnClickListener { findNavController().navigate(R.id.action_moreFragment_to_expenseList) }
        paymentList.setOnClickListener { findNavController().navigate(R.id.action_moreFragment_to_paymentList) }
        expense.setOnClickListener { findNavController().navigate(R.id.action_moreFragment_to_expenseFragment) }
    }

    override fun proceedLogout() {

        parent.stopServiceTracker()
        val intent = Intent(requireContext(), LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)

    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}