package com.azinova.smartwifiadminemv.history.history_main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.azinova.commons.Loader
import com.azinova.smartwifiadminemv.R
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.history_main_fragment.*

class HistoryMainFragment : Fragment() {


    private lateinit var viewModel: HistoryMainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.history_main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HistoryMainViewModel::class.java)
        onClick()
    }

    private fun onClick() {


        getHistoryButton.setOnClickListener {
            if(valid()){
                val bundle =Bundle()
                bundle.putString("phone",phoneHistorySearch.text.toString())
                phoneHistorySearch.text.clear()
                findNavController().navigate(R.id.action_historyMainFragment_to_historyFragment,bundle)
            }
            else{
                Snackbar.make(historyMainOuter, "Please enter valid phone number", Snackbar.LENGTH_SHORT).show()}
        }

    }


    private fun valid(): Boolean {
        return phoneHistorySearch.text.toString().length == 10
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}