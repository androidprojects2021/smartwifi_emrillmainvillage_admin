package com.azinova.smartwifiadminemv.history.history_list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.history.DataItem
import com.azinova.smartwifiadminemv.R
import kotlinx.android.synthetic.main.adapter_history.view.*

class HistoryAdapter(val context: Context,val historyClick: HistoryClick):RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    var data: List<DataItem?>? = listOf()

    inner class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        val numberCoupon = itemView.numberCoupon
        val purchaseDateCoupon= itemView.purchaseDateCoupon
        val statusCoupon= itemView.statusCoupon
        val validityText= itemView.validityText

        init {
            itemView.setOnClickListener { historyClick.clicked(data?.get(position)) }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_history,parent,false))
    }

    override fun getItemCount(): Int {
        return data?.size ?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.numberCoupon.setText(data?.get(position)?.voucherName ?:"")
        holder.purchaseDateCoupon.setText(data?.get(position)?.sellDate ?:"")
        holder.statusCoupon.setText(data?.get(position)?.status ?:"")
        holder.validityText.setText(data?.get(position)?.validity ?:"")
    }

    fun refreshData(data:  List<DataItem?>?){
        this.data = data
        notifyDataSetChanged()
    }


    interface HistoryClick{
        fun clicked(dataItem: DataItem?)
    }

}