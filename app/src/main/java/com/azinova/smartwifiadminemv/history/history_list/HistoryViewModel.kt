package com.azinova.smartwifiadminemv.history.history_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.history.InputHistory
import com.azinova.model.input.resend_sms.InputResendSms
import com.azinova.model.response.history.ResponseHistory
import com.azinova.model.response.resend_sms.ResponseResendSms
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class HistoryViewModel : ViewModel() {


    private val _historyResponse = MutableLiveData<ResponseHistory>()
    val historyResponse : LiveData<ResponseHistory>
        get() = _historyResponse


    private val _sendSmsResponse = MutableLiveData<ResponseResendSms>()
    val sendSmsResponse : LiveData<ResponseResendSms>
        get() = _sendSmsResponse

    fun sendSmsResponseClear(){
        _sendSmsResponse.value = ResponseResendSms(status = "idle")
    }

    fun historyResponseClear(){
        _historyResponse.value = ResponseHistory(status = "idle")
    }

    fun callHistory(inputHistory: InputHistory) {
        viewModelScope.launch {
            try {

                val response =  App().retrofit.history(inputHistory)
                _historyResponse.value = response

            } catch (e: Exception) {
                _historyResponse.value = ResponseHistory(message = "Something went wrong" ,status = "Error")
            }
        }

    }

    fun callResendSms(inputResendSms: InputResendSms) {

        viewModelScope.launch {
            try {

                val response =  App().retrofit.resendSms(inputResendSms)
                _sendSmsResponse.value = response

            } catch (e: Exception) {
                _sendSmsResponse.value = ResponseResendSms(message = "Something went wrong" ,status = "Error")
            }
        }

    }


}