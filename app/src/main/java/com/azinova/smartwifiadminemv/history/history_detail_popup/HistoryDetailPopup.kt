package com.azinova.smartwifiadminemv.history.history_detail_popup

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.azinova.model.response.history.DataItem
import com.azinova.smartwifiadminemv.R

class HistoryDetailPopup(
    val context: Context,
    val dataItem: DataItem?,
    val phoneNo: String,
    val historyDetailsClicks: HistoryDetailsClicks
) {

    private lateinit var builder: AlertDialog.Builder
    private lateinit var customLayout: View
    private lateinit var dialog: AlertDialog

    fun showPopup() {
        try {
            builder = AlertDialog.Builder(context)
            customLayout = LayoutInflater.from(context).inflate(R.layout.history_detail_popup, null)
            builder.setView(customLayout)
            dialog = builder.create()
            dialog.setCancelable(false)

            setData(dataItem)

            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun setData(dataItem: DataItem?) {
        val resendSms = customLayout.findViewById<Button>(R.id.hisDetailsResendSMSButton)
        val close = customLayout.findViewById<Button>(R.id.hisDetailsCloseButton)
        val hisDetailsPhoneNumber = customLayout.findViewById<TextView>(R.id.hisDetailsPhoneNumber)
        val hisDetailsVoucher = customLayout.findViewById<TextView>(R.id.hisDetailsVoucher)
        val hisDetailsDate = customLayout.findViewById<TextView>(R.id.hisDetailsDate)
        val validDate = customLayout.findViewById<TextView>(R.id.validDate)

        validDate.text = dataItem?.validity ?:""
        hisDetailsDate.text = dataItem?.sellDate ?:""
        hisDetailsVoucher.text = dataItem?.voucherName ?:""
        hisDetailsPhoneNumber.text = phoneNo


        resendSms.setOnClickListener {
            historyDetailsClicks.sendMessage(dataItem)
            hidePopup()
        }
        close.setOnClickListener { hidePopup() }

    }

    fun hidePopup() {
        try {
            dialog.dismiss()
        } catch (e: Exception) {
        }
    }


    interface HistoryDetailsClicks{
        fun sendMessage(dataItem: DataItem?)
    }


}