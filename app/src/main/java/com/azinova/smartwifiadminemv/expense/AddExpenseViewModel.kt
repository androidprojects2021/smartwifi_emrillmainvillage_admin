package com.azinova.smartwifiadminemv.expense

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.expense_add.InputExpenseAdd
import com.azinova.model.input.validity.InputValidity
import com.azinova.model.response.expense_add.ResponseExpenseAdd
import com.azinova.model.response.expense_category.ResponseExpenseCategory
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class AddExpenseViewModel : ViewModel() {

    private val _categoryResponse = MutableLiveData<ResponseExpenseCategory>()
    val categoryResponse: LiveData<ResponseExpenseCategory>
        get() = _categoryResponse

    private val _addExpense = MutableLiveData<ResponseExpenseAdd>()
    val addExpense:LiveData<ResponseExpenseAdd>
    get() = _addExpense


    fun clearCategoryResponse(){
        _categoryResponse.value = ResponseExpenseCategory(status = "idle")
    }
    fun clearAddExpenseResponse(){
        _addExpense.value = ResponseExpenseAdd(status = "idle")
    }

    fun expenseCategoryApi(inputValidity: InputValidity){

        viewModelScope.launch {
            try {

                val response = App().retrofit.getExpenseCategory(inputValidity)
                _categoryResponse.value =response

            } catch (e: Exception) {

                _categoryResponse.value = ResponseExpenseCategory(message = "Something went wrong" ,status = "Error")
            }
        }

    }


    fun addExpense(inputExpenseAdd: InputExpenseAdd){
        viewModelScope.launch {
        try {
            val response = App().retrofit.addExpense(inputExpenseAdd)
            _addExpense.value = response

        }catch (e:Exception){
            _addExpense.value = ResponseExpenseAdd(message = "Something went wrong" ,status = "Error")
        }
    }
    }



}