package com.azinova.smartwifiadminemv.expense.list_expense

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.report.InputReport
import com.azinova.model.response.expense_list.ResponseExpenseList
import com.azinova.smartwifiadminemv.App
import kotlinx.coroutines.launch

class ExpenseListViewModel : ViewModel() {

     private val _expenseList = MutableLiveData<ResponseExpenseList>()
  val expenseList : LiveData<ResponseExpenseList>
      get() = _expenseList


    fun payListResponseClear(){
        _expenseList.value = ResponseExpenseList(status = "idle")
    }


    fun getExpenseList(inputReport: InputReport){

        viewModelScope.launch {
            try {

                val response = App().retrofit.getExpenseList(inputReport)
                _expenseList.value = response

            } catch (e: Exception) {
                Log.e("Exec",e.toString())
                _expenseList.value = ResponseExpenseList(message = "Something went wrong" ,status = "Error")

            }
        }

    }

}