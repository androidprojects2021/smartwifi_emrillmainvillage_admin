package com.azinova.smartwifiadminemv.expense

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.azinova.commons.Loader
import com.azinova.model.input.expense_add.InputExpenseAdd
import com.azinova.model.input.validity.InputValidity
import com.azinova.model.response.expense_category.DataItem
import com.azinova.smartwifiadminemv.App
import com.azinova.smartwifiadminemv.R
import com.azinova.smartwifiadminemv.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.add_expense_fragment.*
import java.util.*


class AddExpenseFragment : Fragment() {

    private lateinit var viewModel: AddExpenseViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.add_expense_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddExpenseViewModel::class.java)

        getExpenseCategory()
        clicks()
        observe()
    }

    private fun observe() {

        viewModel.categoryResponse.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if(it.status.equals("success")){

                if(it?.data?.size!! >0) {
                    val adapter = AdapterExpenseCategory(requireContext(),R.layout.custom_spinner, it.data ?: listOf() )
                    validitySpinner.adapter=adapter
                }else {
                    Snackbar.make(outerExpAdd, "No validity available", Snackbar.LENGTH_SHORT).show()
                }
                viewModel.clearCategoryResponse()
            }
            else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerExpAdd, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }
        })


        viewModel.addExpense.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if(it.status.equals("success")){

               Toast.makeText(requireContext(),it.message,Toast.LENGTH_SHORT).show()
                viewModel.clearAddExpenseResponse()
                datePicked.text = "Select Date"
                datePicked.setTextColor(Color.GRAY)
                supplierAddExpense.text.clear()
                descriptionAddExpense.text.clear()
                amountAddExpense.text.clear()

            }
            else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerExpAdd, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })


    }

    private fun clicks() {

        dateSelector.setOnClickListener {

            val cldr: Calendar = Calendar.getInstance()
            val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
            val month: Int = cldr.get(Calendar.MONTH)
            val year: Int = cldr.get(Calendar.YEAR)
           val datepicker = DatePickerDialog(requireContext(), OnDateSetListener {
                   view, year, monthOfYear, dayOfMonth ->
               datePicked.text ="$year-${if((monthOfYear+1) <10) "0"+(monthOfYear+1) else (monthOfYear+1)}-${if(dayOfMonth<10) "0"+dayOfMonth else dayOfMonth}"
               datePicked.setTextColor(Color.BLACK)

           }, year, month, day
            )
            datepicker.show()

        }

        submitButton.setOnClickListener {

            if(valid()){

            Loader.showLoader(requireContext())
            val category :DataItem?  = validitySpinner.selectedItem as DataItem
            viewModel.addExpense(InputExpenseAdd(amountAddExpense.text.toString(),
                App().sharefPrefs.user_details?.hotspotSessionId ,
                category?.id,
                App().sharefPrefs.user_details?.hotspotSessionName,
                App().sharefPrefs.user_details?.userId,
                supplierAddExpense.text.toString(),
                datePicked.text.toString(),
                descriptionAddExpense.text.toString(),
                App().sharefPrefs.user_details?.token))
            }

        }



    }

    private fun valid(): Boolean {

        val category :DataItem? = validitySpinner.selectedItem as DataItem

        if(datePicked.text.toString().equals("Select Date")){
            Snackbar.make(outerExpAdd, "Please select date", Snackbar.LENGTH_SHORT).show()
            return false }
        if(category ==null){
            Snackbar.make(outerExpAdd, "Please select category", Snackbar.LENGTH_SHORT).show()
            return false
        }

        if(supplierAddExpense.text.toString().isNullOrBlank()){
            Snackbar.make(outerExpAdd, "Please enter supplier", Snackbar.LENGTH_SHORT).show()
            return false
        }
           if(descriptionAddExpense.text.toString().isNullOrBlank()){
               Snackbar.make(outerExpAdd, "Please enter description", Snackbar.LENGTH_SHORT).show()
            return false
        }
           if(amountAddExpense.text.toString().isNullOrBlank()){
               Snackbar.make(outerExpAdd, "Please enter amount", Snackbar.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    private fun getExpenseCategory() {
        viewModel.expenseCategoryApi(InputValidity(App().sharefPrefs.user_details?.hotspotSessionId,
                App().sharefPrefs.user_details?.hotspotSessionName,
            App().sharefPrefs.user_details?.userId, App().sharefPrefs.user_details?.token))

    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}