package com.azinova.smartwifiadminemv.expense

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.azinova.model.response.expense_category.DataItem
import com.azinova.smartwifiadminemv.R

class AdapterExpenseCategory (context: Context, val layout:Int, val data:List<DataItem?>): ArrayAdapter<DataItem>(context,layout,data) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var currentItemView = convertView
        if (currentItemView == null) {
            currentItemView = LayoutInflater.from(context).inflate(R.layout.custom_spinner, parent, false)
        }

        val textView1 = currentItemView!!.findViewById<TextView>(R.id.textSpin)
        textView1.text = data[position]?.categoryName

        return currentItemView

    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return createViewFromResource(position, convertView, parent)
    }

    private fun createViewFromResource(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layout, parent, false) as TextView
        view.text = data[position]?.categoryName
        return view
    }



}